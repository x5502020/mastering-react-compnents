import Header from "./Header";
import Footer from "./Footer";
import Photos from "./Photos";
import photoData from "../data/photoData";

const Page = () => {
  return (
    <div>
      <Header title="Header" />
      <main>
        <Photos photos={photoData} />
      </main>
      <Footer title="Footer" />
    </div>
  );
};

export default Page;
