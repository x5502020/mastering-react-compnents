import PropTypes from "prop-types";
import "./Photos.css";

const Photos = ({ photos }) => {
  const firstTenPhotos = photos.slice(0, 10);

  return (
    <div>
      <h2>Photos</h2>
      <ul className="photos-container">
        {firstTenPhotos.map((photo) => (
          <li key={photo.id} className="photo-item">
            <img src={photo.url} alt={photo.title} className="photo-img" />
            <p className="photo-title">{photo.title}</p>
          </li>
        ))}
      </ul>
    </div>
  );
};

Photos.propTypes = {
  photos: PropTypes.arrayOf(
    PropTypes.shape({
      albumId: PropTypes.number.isRequired,
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      thumbnailUrl: PropTypes.string.isRequired,
    })
  ).isRequired,
};

export default Photos;
